document.addEventListener("DOMContentLoaded", function() {
	if (typeof(Storage) !== "undefined") {

		if ((localStorage.getItem("name") === 0) || (localStorage.getItem("name") == null)) {

			var name = prompt("Cual es tu nombre?");
			localStorage.setItem("name", name);
			sessionStorage.setItem("contador", 0);
			document.getElementById("saludo").innerHTML = "Primera visita, " + localStorage.getItem("name") + "!";
			document.getElementById("contador").innerHTML = "Contador: " + sessionStorage.getItem("contador");

			document.getElementById("incrementar").addEventListener("click", incrementar, false);
			document.getElementById("decrementar").addEventListener("click", decrementar, false);
			document.getElementById("logout").addEventListener("click", function() {
				try {
					comprobarLogin();
					borrar();
				} catch (err) {
					alert(err);
				}

			}, false);
		} else if (localStorage.getItem("name") != 0 && (localStorage.getItem("name") != null)) {
			sessionStorage.setItem("contador", 0);
			document.getElementById("saludo").innerHTML = "Bienvenido de nuevo, " + localStorage.getItem("name") + "!";
			document.getElementById("contador").innerHTML = "Contador: " + sessionStorage.getItem("contador");

			document.getElementById("incrementar").addEventListener("click", incrementar, false);
			document.getElementById("decrementar").addEventListener("click", decrementar, false);
			document.getElementById("logout").addEventListener("click", function() {
				try {
					comprobarLogin();
					borrar();
				} catch (err) {
					alert(err);
				}

			}, false);
		}
	} else {
		alert("Lo sentimos, su navegador no soporta WebStorage.");
	}

}, false);

function incrementar() {
	var contador = parseInt(sessionStorage.getItem("contador")) + 1;
	sessionStorage.setItem("contador", contador);
	document.getElementById("contador").innerHTML = "Contador: " + sessionStorage.getItem("contador");
}

function decrementar() {
	var contador = parseInt(sessionStorage.getItem("contador")) - 1;
	sessionStorage.setItem("contador", contador);
	document.getElementById("contador").innerHTML = "Contador: " + sessionStorage.getItem("contador");
}

function borrar() {
	if (localStorage.getItem("name") != null) {
		alert("Se ha cerrado la sesion " + localStorage.getItem("name"));
	}
	localStorage.clear();
	document.getElementById("saludo").innerHTML = "";
	document.getElementById("contador").innerHTML = "";
}

function comprobarLogin() {
	if (localStorage.getItem("name") == null) {
		throw "No existe un usuario logueado.";
		return false
	} else {
		return true;
	}
}