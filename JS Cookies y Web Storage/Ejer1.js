document.addEventListener("DOMContentLoaded", function() {
	var ver = document.getElementById("ver");
	var crear = document.getElementById("crear");
	var modificar = document.getElementById("modificar");
	var leer = document.getElementById("leer");
	var borrar = document.getElementById("borrar");

	crear.addEventListener("click", crearCck, false);

	ver.addEventListener("click", verCck, false);

	modificar.addEventListener("click", function() {
		try {
			modificarCck();
		} catch (error) {
			alert(error);
		}
	}, false);

	leer.addEventListener("click", function() {
		try {
			leerCck();
		} catch (error) {
			alert(error);
		}
	}, false);

	borrar.addEventListener("click", function() {
		try {
			borrarCck();
		} catch (error) {
			alert(error);
		}
	}, false);

}, false);

function crearCck() {
	var cckNombre = prompt("Nombre de la cookie:");
	var cckValor = prompt("Valor de la cookie:");
	var cckDiasEx = prompt("Introduce número de días para la expiración:");
	var expirationDay = new Date();
	var diaActual = expirationDay.getDate();
	var diaFinal = parseInt(cckDiasEx);
	expirationDay.setDate(diaActual + diaFinal);
	document.cookie = cckNombre + "=" + cckValor + "; expiration=" + expirationDay.getDate() + ";";
}

function verCck() {
	var cck = document.cookie;
	alert(cck);
}

function modificarCck() {
	var cckOriginal = document.cookie;

	if (!cckOriginal) {
		throw "No existen cookies a modificar.";
		return;
	}

	var cckNombre = prompt("Nombre de la cookie:");
	var cckDeseada = document.cookie.match(new RegExp(cckNombre + '=([^;]+)'));

	if (obtenerCookie(cckNombre) != "" && cckDeseada) {
		var cckValor = prompt("Valor de la cookie:");
		var cckDiasEx = prompt("Introduce número de días para la expiración:");
		var expirationDay = new Date();
		var diaActual = expirationDay.getDate();
		var diaFinal = parseInt(cckDiasEx);
		expirationDay.setDate(diaActual + diaFinal);
		document.cookie = cckNombre + "=" + cckValor + "; expiration=" + expirationDay.getDate() + ";";
		alert("Cookie modificada correctamente.");
	}else{
		throw "No existen esa cookie o has cancelado.";
		return;
	}

}

function leerCck() {
	var cckOriginal = document.cookie;

	if (!cckOriginal) {
		throw "No existen cookies a leer.";
		return;
	}

	var cckNombre = prompt("Nombre de la cookie:");
	var cckDeseada = document.cookie.match(new RegExp(cckNombre + '=([^;]+)'));

	if (cckDeseada) {
		alert("Valor: " + obtenerCookie(cckNombre).substring(1));
	} else {
		throw "No existe esa cookie o has cancelado.";
		return;
	}

}

function borrarCck(){
	var cckOriginal = document.cookie;

	if (!cckOriginal) {
		throw "No existen cookies a borrar.";
		return;
	}

	var cckNombre = prompt("Nombre de la cookie:");
	var cckDeseada = document.cookie.match(new RegExp(cckNombre + '=([^;]+)'));

	if (cckDeseada) {
		var diaDeleter = new Date(1970, 1, 1);
		document.cookie = cckNombre +"=; expires="+diaDeleter+";";
		alert("Cookie borrada correctamente.");
	} else {
		throw "No existe esa cookie o has cancelado.";
		return;
	}
}

function obtenerCookie(nombre) { //FUNCIÓN QUE RETORNA EL VALOR DE UNA COOKIE PASADO EL NOMBRE DE ESA COCKIE COMO PARÁMETRO
	var name = nombre;
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') c = c.substring(1);
		if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
	}
	return "";
}