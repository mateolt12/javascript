$(document).ready(function() {
	$("#crearparrafo").click(crearParrafo);
	$("#crearimagen").click(crearImagen);
	$("#borrarultimo").click(borrarUltimo);
	$("#borrarprimero").click(borrarPrimero);
	$("#sustituirprimero").click(sustituirPrimero);
});

function crearParrafo() {
	var texto = $("#texto").val();
	if (texto != "") {
		var nuevoP = $("<p></p>").text(texto);
		$("#div1").append(nuevoP);
		$("#div1").addClass("rojo");
	} else {
		alert("El textarea está vacío. Introduzca texto.");
	}

}

function borrarUltimo() {
	var contenedor = $("#div1");
	if (contenedor.text() != "") {
		contenedor.children().last().remove();

	} else {
		alert("El contenedor está vacío, no hay nada que borrar.");
	}

}

function borrarPrimero() {
	var contenedor = $("#div1");
	if (contenedor.text() != "") {
		contenedor.children().first().remove();
	} else {
		alert("El contenedor está vacío, no hay nada que borrar.");
	}
}

function sustituirPrimero() {
	var contenedor = $("#div1");
	if (contenedor.text() != "") {
		var nuevoP = $("<p></p>").text("Vacío");
		contenedor.children().first().replaceWith(nuevoP);
	} else {
		alert("El contenedor está vacío, no hay nada que sustituir.");
	}
}

function crearImagen() {
	var contenedor = $("#div1");
	var src = $("#texto").val();
	if (src != "") {
		var alt = prompt("Introduzca nombre de la foto.");
		contenedor.append($("<img/>", {
			"src": src,
			"alt": alt,
		}));
	} else {
		alert("El textarea está vacío. Introduzca url de imagen y a continuación un 'alt' en la caja de contenido.");
	}
}