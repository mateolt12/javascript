$(document).ready(function() {
	var cambio = false;
	while(!cambio)
		try {
			validacion();
			cambio = true;
		} catch (err) {
			alert(err);
		}

	});

function validacion() {
	var filas = parseInt(prompt("Introduzca el número de filas."));
	var columnas = parseInt(prompt("Introduzca el número de columnas."));

	if (!isNaN(filas) && !isNaN(columnas) && (filas > 0) && (columnas > 0)) {
		crearTabla(filas, columnas);
	} else {
		throw "Ambos valores han de ser de tipo númerico entero y mayores a 0.";
	}
	return;
}

function crearTabla(filas, columnas) {
	var tabla = $("<table>");
	tabla.addClass("table");
	tabla.addClass("table-dark");
	tabla.addClass("table-bordered");
	tabla.addClass("table-hover");

	for (let i = 0; i < filas; i++) {
		var tr = $("<tr>");
		for (let j = 0; j < columnas; j++) {
			var td = $("<td>").text("celda en la fila " + (i+1) + " columna " + (j+1));
			td.click(activar);
			tr.append(td);
		}
		tabla.append(tr);
	}
	$("#contenedor").append(tabla);
}

function activar(){
	var input = $("<input></input>");
	input.attr("type", "text");
	$(event.target).contents().replaceWith(input);
	$(event.target).children().last().focus();
	 $(event.target).off();
	input.keypress(salvarCambios);

}

function salvarCambios(e){
	if(e.which === 13){
		var padre = $(event.target).parent();
		var valor = $(event.target).val();
		$(event.target).off('keypress', salvarCambios);
		$(event.target).replaceWith(valor)
		padre.click(activar);
	}
	

}

