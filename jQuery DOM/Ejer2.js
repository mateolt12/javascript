$(document).ready(function() {
	$("#anadir").click(anadirElemento);

});

function anadirElemento() {
	var texto = $("#texto").val().toLowerCase().substr(0, 1).toUpperCase() + $("#texto").val().substr(1);
	var nuevo = $("<li></li>").text(texto);

	var lista = $("#lista");

	$("#lista").children().each(function() {
		if ($(this).text() > texto) {
			nuevo.insertBefore($(this));
			return false;
		} else {
			lista.append(nuevo);
		}
	});
}