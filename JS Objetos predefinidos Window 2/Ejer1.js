window.onload = function() {

	document.getElementById("abrir").onclick = abrir;
	document.getElementById("cerrar").onclick = cerrar;

}

function valorRadio() {


	var nPestana = document.getElementById("nPestana").checked;
	var nVentana = document.getElementById("nVentana").checked;
	var checkeado;

	if (nPestana) {
		checkeado = true;
	} else {
		checkeado = false;
	}

	return checkeado;

}

function getUrl() {

	var urlSelected = document.getElementById("sitios");
	var enlace;

	switch (urlSelected.value) {
		case "eldiario":
			enlace = "https://www.eldiario.es";
			break;

		case "elpais":
			enlace = "https://www.elpais.es";
			break;

		case "elmundo":
			enlace = "https://www.elmundo.es";
			break;

		case "publico":
			enlace = "https://www.publico.es";
			break;
	}

	return enlace;

}

function abrir() {

	var radioCheckeado = valorRadio();

	if (radioCheckeado) {

		nPestana = window.open(getUrl(), getUrl());
	} else {
		nVentana = window.open(getUrl(), getUrl(), "height=600,width=800");
	}



}


function cerrar() {

	if (nPestana) {
		nPestana.close();
	}

	if (nVentana) {
		nVentana.close();
	}

}