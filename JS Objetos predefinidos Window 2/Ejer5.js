var reloj;

window.onload = function() {

	document.getElementById("iniciar").setAttribute("onclick", "controlador('iniciar')");
	document.getElementById("parar").setAttribute("onclick", "controlador('parar')");
	document.getElementById("continuar").setAttribute("onclick", "controlador('continuar')");
	document.getElementById("reset").setAttribute("onclick", "controlador('reset')");

}


function controlador(instruccion) {
	switch (instruccion) {
		case "iniciar":
			iniciar();
			break;
		case "parar":
			parar();
			break;
		case "continuar":
			iniciar();
			break;
		case "reset":
			reset();
			break;
	}
}


function iniciar() {

	clearTimeout(reloj);
	document.getElementById("iniciar").style.display = "none";
	document.getElementById("cuenta").value++;
	reloj = setTimeout(iniciar, 1000);
}

function parar() {

	clearTimeout(reloj);
}


function reset() {
	clearTimeout(reloj);
	document.getElementById("cuenta").value = -1;
	document.getElementById("cuenta").value++;;
	reloj = setTimeout(iniciar, 1000);
}