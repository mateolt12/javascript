window.onload = function() {

	document.getElementById("lanzar").onclick = mostrarDado;

}

function aleatorio(min, max) {

	return Math.round(Math.random() * (max - min)+min);

}

function mostrarSubventana() {

	return window.open("", "resultado", "resizable=yes");
}


function mostrarDado() {

	var ventana = mostrarSubventana();
	var documentoVentana = ventana.document.open();
	var numAleatorio = aleatorio(1, 6);

	documentoVentana.write("<html><head><title>Resultado</title></head><body><img id='imagen' alt='dado 1'/><br><button id='cerrar'>Cerrar subventana</button></body></html>");
	documentoVentana.getElementById('cerrar').onclick = function() {
		ventana.close();
	}

	switch (numAleatorio) {
		case 1:
			ventana.document.getElementById('imagen').setAttribute("src", "images/dado1.png");

			break;

		case 2:
			ventana.document.getElementById('imagen').setAttribute("src", "images/dado2.png");

			break;

		case 3:
			ventana.document.getElementById('imagen').setAttribute("src", "images/dado3.png");

			break;

		case 4:
			ventana.document.getElementById('imagen').setAttribute("src", "images/dado4.png");

			break;

		case 5:
			ventana.document.getElementById('imagen').setAttribute("src", "images/dado5.png");

			break;

		case 6:
			ventana.document.getElementById('imagen').setAttribute("src", "images/dado6.png");

			break;
	}

}