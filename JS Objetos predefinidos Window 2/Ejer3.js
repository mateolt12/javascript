var subventana;

window.onload = function() {
	document.getElementById("abrirsubventana").onclick = abrirSubVentana;
	document.getElementById("cerrarsubventana").onclick = cerrarSubVentana;

}

function generarSubVentana() {
	subventana = open("", "Sub-ventana", "resizable=yes");
	return subventana;
}

function abrirSubVentana(){
	subventana = generarSubVentana();
	documentoSubventana = subventana.document.open();
	var codigoHTMLSub = "<h1>Sub-ventana</h1>Introduzca texto a copiar en la ventana principal: <input type='text' id='textosubventana'><br><br><button id='enviaraventana'>Enviar texto a la ventana padre</button>";
	documentoSubventana.write(codigoHTMLSub);
	documentoSubventana.getElementById("enviaraventana").onclick = obtenerTexto;

}


function cerrarSubVentana() {
	if (subventana) {
		subventana.close();

	} else {

		alert("No existe ventana que cerrar :(");
	}

}

function controlVacio(texto) {
	if (texto == "") {
		return false;
	} else {
		return true;
	}
}

function obtenerTexto() {
	var textoObtenido = subventana.document.getElementById("textosubventana").value;
	if (controlVacio(textoObtenido)) {
		subventana.opener.document.getElementById("texto").value = textoObtenido;
	} else {
		subventana.alert("No has introducido ningun valor a enviar aquí.");
	}

}