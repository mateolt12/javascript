window.onload = function() {

	principal();

}
//FUNCIÓN PRINCIPAL LLAMADA POR EL WINDOW.ONLOAD QUE DECLARA EL ARRAY INICIAL Y LLAMA A LA FUNCION ARREGLARPAISES PARA TRABAJAR LA POSTERIOR SALIDA Y FORMATEO.
function principal() {
	var paises = new Array("España", "Francia", "Suecia", "Italia", "Noruega", "Portugal", "Holanda");
	formateaSalida(paises);

	var nuevosPaises = arreglarPaises(paises);
	formateaSalida(nuevosPaises);
}

//FUNCIÓN QUE BORRA EL PAÍS DE ESPAÑA Y AÑADE LOS NUEVOS PÁISES LOS CUALES ESTÁN ALMACENADOS EN OTRO ARRAY.
function arreglarPaises(paises) {

	var modificar = paises;
	modificar.shift();

	var nuevosPaises = new Array("Alemania", "Dinamarca", "Grecia", "Suiza");
	modificar = modificar.concat(nuevosPaises);
	ordenarPaises(modificar);
	añadirBelgica(modificar);

	return modificar;

}

//FUNCIÓN QUE ORDENA EL ARRAY PAISES QUE LE PASAMOS COMO PARÁMTERO POR ORDEN ALFABÉTICO.
function ordenarPaises(paises) {
	paises.sort();
}

//FUNCIÓN QUE AÑADE EL PAÍS DE BÉLGICA COMO PRIMER VALOR DE LA ENUMERACIÓN.
function añadirBelgica(paises) {
	paises.unshift("Belgica");
}

//FUNCION QUE CONTROLA EL USO DE LA "," O DEL "." DEPENDIENDO DE LA POSICIÓN DEL ELEMENTO EN LA ENUMERACIÓN DE LOS PAÍSES.
function formateaSalida(paisesArray) {

	for (let i in paisesArray) {
		if (i == (paisesArray.length - 1)) {
			document.getElementById("visualizar").innerHTML += paisesArray[i] + ".<br>";

		} else {
			document.getElementById("visualizar").innerHTML += paisesArray[i] + ", ";
		}

	}

}