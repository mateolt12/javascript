var datos = new Array();
datos[0] = new Array("Fran", "DWCS", 10);
datos[1] = new Array("Rodri", "DWCC", 10);
datos[2] = new Array("Iván", "DIW", 9);
datos[4] = new Array("Gamallo", "DDA", 9);

window.onload = function() {
	document.getElementById("mostrarProfesor").onclick = mostrarProfesor;
	document.getElementById("mostrarAlumnos").onclick = mostrarAlumnos;
	document.getElementById("masAlumnos").onclick = masAlumnos;
	document.getElementById("menosAlumnos").onclick = menosAlumnos;
	document.getElementById("mediaAlumnos").onclick = mediaAlumnos;
	construirTabla(datos);

}

function construirTabla(datos) {
	var tabla = "<table id='tablaModulos'><tr><th>Profesor</th><th>Asignatura</th><th>Alumnos</th></tr>";

	for (i in datos) {
		tabla += "<tr><td>" + datos[i][0] + "</td><td>" + datos[i][1] + "</td><td>" + datos[i][2] + "</td></tr>";
	}

	tabla += "</table>";

	document.getElementById("visualizar").innerHTML = tabla;
}

function borrarTabla(id) {
	var eliminar = document.getElementById(id);
	eliminar.parentNode.removeChild(eliminar);
}

function mostrarProfesor() {
	var asignatura = document.getElementById("listaAsignaturas").value;
	var profesor = "";

	for (i in datos) {
		if (asignatura == datos[i][1]) {
			profesor = datos[i][0];
			break;
		}

	}
	document.getElementById("visualizar2").innerHTML = profesor;

}

function mostrarAlumnos() {
	var asignatura = document.getElementById("listaAlumnos").value;
	var numAlumnos = 0;

	for (i in datos) {
		if (asignatura == datos[i][1]) {
			numAlumnos = datos[i][2];
			break;
		}

	}
	document.getElementById("visualizar3").innerHTML = numAlumnos;

}

function masAlumnos() {
	var alumnosMax = datos[0][2];
	var nombreModulo = datos[0][1];
	for (i in datos) {
		if (datos[i][2] > alumnosMax) {
			alumnosMax = datos[i][2];
			nombreModulo = datos[i][1];
		}
	}

	document.getElementById("visualizar4").innerHTML = nombreModulo;
}

function menosAlumnos() {
	var alumnosMin = datos[0][2];
	var nombreProfesor = datos[0][0];

	for (i in datos) {
		if (datos[i][2] < alumnosMin) {
			alumnosMin = datos[i][2];
			nombreProfesor = datos[i][0]
		}
	}
	document.getElementById("visualizar5").innerHTML = nombreProfesor;
}

function mediaAlumnos() {
	var sumaAlumnos = 0;
	var contador = 0;

	for (i in datos){
		sumaAlumnos+=datos[i][2];
		contador++;
	}

	var media = sumaAlumnos/contador;

	document.getElementById("visualizar6").innerHTML = media;
}