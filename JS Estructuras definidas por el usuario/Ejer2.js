var alumnos = new Array("Juan Carlos", "Aida", "Mateo", "Luismi", "Santi", "Alejo", "Miguel", "Brais", "Nogue", "Rocío");
var idades = new Array(32, 26, 23, 31, 28, 22, 19, 19, 26, 45);

window.onload = function() {


	principal();


}

function principal() {
	//var alumnos = new Array("Juan Carlos", "Aida", "Mateo", "Luismi", "Santi", "Alejo", "Miguel", "Brais", "Nogue", "Rocío");
	//var idades = new Array(32, 26, 23, 31, 28, 22, 19, 19, 26, 45);
	document.getElementById("ordenar").setAttribute("onclick", "ordenar('alumnos', 'idades')");
	construirTabla(alumnos, idades);
	var tabla = document.getElementById("sinOrden");

}

function construirTabla(alumnos, idades) {
	var tabla = "<table id='sinOrden'><tr><th>Alumnos</th><th>Idades</th></tr>";

	for (i in alumnos) {
		tabla += "<tr><td>" + alumnos[i] + "</td><td>" + idades[i] + "</td></tr>";
	}

	tabla += "</table>";

	document.getElementById("visualizar").innerHTML = tabla;
}

function ordenar() {
	borrarTabla("sinOrden");

	var alum = alumnos;
	var idad = idades;
	var AlumRet = new Array();
	var IdadeRet = new Array();

	var Idademenor = idad[0];
	var Alummenor = alum[0];



	for (i in idad) {
		if (idad[i] < Idademenor) {
			Idademenor = idad[i];
			Alummenor = alum[i];
			IdadeRet.push(Idademenor);
			AlumRet.push(Alummenor);
		}


	}
	construirTabla(AlumRet, IdadeRet);

	//	return ordenados;

}

function borrarTabla(id) {
	var eliminar = document.getElementById(id);
	eliminar.parentNode.removeChild(eliminar);
}