var lista = new Array();
var contador = 0;

window.onload = function() {
	document.getElementById("añadir").onclick = recojerTexto;
	document.getElementById("terminar").onclick = terminar;

}

function validacion(texto) {
	if (!isNaN(texto)) {
		throw "Palabra no válida. Introduzca caracteres alfabéticos A-Z, Ñ.";
		return false;
	} else {
		return true;
	}
}

function recojerTexto() {
	var texto = document.getElementById("texto").value;
	try {
		if (validacion(texto)) {
			lista.push(texto);
			contador++;
			document.getElementById("visualizar").innerHTML = "Elemento " + texto + " añadido correctamente.<br>Has introducido " + contador + " palabras.";
		}
	} catch (err) {
		document.getElementById("visualizar").innerHTML = err;
	}

}

function terminar(){

	var codigoLista = "Has introducido los siguientes elementos:<br><ol type='1'>";
	for(i in lista){
		codigoLista+="<li>"+lista[i].toUpperCase()+"</li>";
	}
	codigoLista+="</ol>";
	document.getElementById("visualizar").innerHTML = codigoLista;
	
}