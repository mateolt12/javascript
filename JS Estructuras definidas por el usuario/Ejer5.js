var lista = new Array();
lista[0] = new Array(1, 11111111, 591227);
lista[1] = new Array(1, 22222222, 591227);
lista[2] = new Array(1, 33333333, 591227);

window.onload = function() {
	document.getElementById("aplicarArriba").onclick = aplicarArriba;
	document.getElementById("aplicarAbajo").onclick = aplicarAbajo;
	// document.getElementById("aplicarArriba").setAttribute("onclick", "tabla('lista')");
}

function validarInput(idInput) {
	var campo = document.getElementById(idInput).value.toString();
	var esValido = false;
	var tipoPeticion = getTipoPeticion();
	switch (idInput) {
		case "codigo":
			if (tipoPeticion == 1) {
				if (campo.length != 8 || campo == "") {
					alert("Error! El código de centro debe tener 8 números.");
				} else {
					esValido = true;
				}
			} else {
				if (campo.length != 9 || campo == "") {
					alert("Error! El código de localidad debe tener 9 números.");
				} else {
					esValido = true;
				}
			}
			break;
		default:
			if (campo < 0 || campo == "") {
				alert("Error! El número debe ser mayor o igual que 0");
			} else {
				esValido = true;
			}
			break;
	}
	return esValido;

}

//Esta función crea la tabla en el HTML cada vez que se aplique un cambio.
function tabla() {
	var tabla = "<table id='tabla' class='table-bordered'><thead class='thazul'><tr><th></th><th>Orden</th><th>Tipo peticion</th><th>Código</th><th>Especialidade</th></tr></thead>";
	for (let i = 0; i < lista.length; i++) {
		tabla += "<tr><td><input type='checkbox' id='check" + i + "' value='check" + i + "'></td><td>" + (i + 1) + "</td><td>" + lista[i][0] + "</td><td>" +
			lista[i][1] + "</td><td>" + lista[i][2] + "</td></tr>";
	}
	tabla += "</table>";
	//return tabla;
	document.getElementById("divtabla").innerHTML = tabla;
}

//Esta función retorna el valor del idInput de posiciones que la halla llamado (no código).
function getPosicionesInputs(idInput) {
	return document.getElementById(idInput).value;
}

//Esta función retorna el valor del input text codigo.
function getCodigo() {
	return document.getElementById("codigo").value;
}

//Esta función retorna 1 si "A centro" o 2 si "A localidade" en el <select>
function getTipoPeticion() {
	return document.getElementById("selectPeticion").value;
}

//Esta función crea un elemento que ocupara una fila de la tabla pasandole los parámetros necesarios.
function crearElemento(parametros) {
	var elemento = new Array(parametros[0], parametros[1], parametros[2]);
	return elemento;
}

//Esta función retorna el elemento de la lista en la posición que se le pasa por parámetro.
function getElemento(posicion) {
	var elemento = new Array(lista[posicion][0], lista[posicion][1], lista[posicion][2]);
	return elemento;
}

//Esta función retorna un array de ids de casillas checkeadas. 
function getCheckeado(check) {
	var checkeado = false;
	if (document.getElementById(check).checked) {
		checkeado = true;
	}

	return checkeado;
}

//Esta función añade al array de elementos un elemento que hemos previamente creado con la funcion crearElemento().
function añadirElementoLista(elemento) {
	lista.push(elemento);
}

//CONTINUAR AQUI. (MIRAR ARRAY.SLICE Y ARRAY.SPLICE)
function añadirElementoPosicionLista(elemento, posicion) {
	if (lista.length == 0) {
		añadirElementoLista(elemento);
	} else {
		lista.splice((posicion - 1), 0, elemento);
	}

}

function substituirElementoPosicionLista(elemento, posicion) {
	if (lista.length == 0) {
		añadirElementoLista(elemento);
	} else {
		lista.splice((posicion - 1), 1, elemento);
	}

}


//Esta función retorna el value que está checkeado del radio group name que le indiquemos como parámetro.
function getRadioChecked(nameRadioGroup) {
	var radioButtons = document.getElementsByName(nameRadioGroup);
	var checkeado;
	for (let i in radioButtons) {
		if (radioButtons[i].checked) {
			checkeado = radioButtons[i].value;
			break;
		}
	}
	return checkeado;
}

//Esta función retorna el array de parametros que usaremos en la funcion crearElemento() como parametros para dicha función o 0 en caso de que la validacion de codigo sea false.
function crearParametrosDeElemento() {
	var parametrosElemento = new Array();
	parametrosElemento[0] = getTipoPeticion();

	parametrosElemento[1] = getCodigo();
	parametrosElemento[2] = 591227;
	return parametrosElemento;


}

//Esta función elimina los elementos que estan checkeados.
function eliminar() {
	for (let i = lista.length - 1; i >= 0; i--) {
		var textoCheck = "check";
		textoCheck += i.toString();
		if (getCheckeado(textoCheck)) {
			lista.splice(i, 1);
		}
	}
}

//Esta función es la que se llamará cuando se pulse el botón de aplicar.
function aplicarArriba() {
	var cualCheckeado = getRadioChecked("engadir");
	var parametrosElemento;
	var elemento;

	switch (cualCheckeado) {
		case "engadirFinal":
			if (validarInput("codigo")) {
				parametrosElemento = crearParametrosDeElemento();
				console.log(typeof(parametrosElemento));
				elemento = crearElemento(parametrosElemento);
				añadirElementoLista(elemento);
				tabla();
			} else {
				alert("Lo sentimos, no se ha podido crear su petición.")
			}
			break;

		case "engadirPosicion":
			if (validarInput("codigo")) {
				parametrosElemento = crearParametrosDeElemento();
				console.log(typeof(parametrosElemento));
				elemento = crearElemento(parametrosElemento);
				posicion = getPosicionesInputs("engadirPos");
				añadirElementoPosicionLista(elemento, posicion);
				tabla();
			} else {
				alert("Lo sentimos, no se ha podido crear su petición");
			}
			break;

		case "substituePosicion":
			if (validarInput("codigo")) {
				parametrosElemento = crearParametrosDeElemento();
				console.log(typeof(parametrosElemento));
				elemento = crearElemento(parametrosElemento);
				posicion = getPosicionesInputs("sustituePos");
				substituirElementoPosicionLista(elemento, posicion);
				tabla();
			} else {
				alert("Lo sentimos, no se ha podido crear su petición");
			}
			break;
	}
}

function aplicarAbajo() {
	var cualCheckeado = getRadioChecked("radioAbajo");
	//	var checkeados = getArrayCheckeados();
	var contador = 0;
	switch (cualCheckeado) {
		case "eliminar":
			habilitarCheckbox();
			eliminar();
			tabla();
			break;
		case "mover":
			moverPosicion(getPosicionesInputs("moverPos"));
			tabla();
			break;
	}
}

function moverPosicion(posicion) {

	var elementoMover = deshabilitarCheckbox();
	var posicionDestino = posicion;
	lista.splice(elementoMover, 1, posicion);


}

function deshabilitarCheckbox() {
	var posicionElemento;
	for (let i = 0; i < lista.length; i++) {
		var textoCheck = "check";
		textoCheck += i.toString();
		if (getCheckeado(textoCheck)) {
			posicionElemento = i;
			break;
		} else {
			document.getElementById(textoCheck).disabled = true;
		}
	}

	for (let j = posicionElemento; j < lista.length; j++) {
		var textoCheck = "check";
		textoCheck += j.toString();
		document.getElementById(textoCheck).disabled = true;
	}

	return posicionElemento;
}

function habilitarCheckbox() {
	for (let i = 0; i < lista.length; i++) {
		var textoCheck = "check";
		textoCheck += i.toString();
		if (getCheckeado(textoCheck)) {
			document.getElementById(textoCheck).disabled = false;
		}
	}
}