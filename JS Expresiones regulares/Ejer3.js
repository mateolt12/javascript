window.onload = function () {
	document.getElementById("test").addEventListener("click", test, false);
	document.getElementById("str").addEventListener("keypress", function (e) {if(e.which==13) test();}, false);
	document.getElementById("re").addEventListener("keypress", function (e) {if(e.which==13) test();}, false);
}


function test() {

	var str = document.getElementById("str").innerHTML;
	re = /\d{8}/gi;
	
	var res = "<h2>Probando... " + re.toString() + "</h2>";
	res += "re.exec = " + re.exec(str);
	res += "<br>re.test = " + re.test(str);
	res += "<br>str.search = " + str.search(re);
	var num_matches = (str.match(re) == null) ? 0 : str.match(re).length;
	res += "<h3>str.match [" + num_matches + "] = " + str.match(re) + "</h3>";
	res += "str.replace = " + str.replace(/\b(\d{4})(\d{2})(\d{2})\b/g,'$3-$2-$1');
	document.getElementById("res").innerHTML = res;
}
