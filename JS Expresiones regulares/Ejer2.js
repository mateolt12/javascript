window.onload = function () {
				document.getElementById("test").addEventListener("click", test, false);
				document.getElementById("str").addEventListener("keypress", function (e) {if(e.which==13) test();}, false);
				document.getElementById("re").addEventListener("keypress", function (e) {if(e.which==13) test();}, false);
			}


			function test() {
				var re = document.getElementById("re").value;
				var str = document.getElementById("str").value;
				//str = "Tres tristes tigres tragaban trigo en tres tristes trastos.";
				
				//re = new RegExp(re, "gi");
				// re = new RegExp("t\\w{3,}", "gi");
				// re = /t\w{3,}/gi;
				// re = /\b[A-ZÑ]{2}\s{1}/gi; Palabras de dos letras.
				// re = /\b[A-ZÑ]{2}[aeiou]{1}[A-ZÑ]{1}\s?/gi; Palabras 4 letras vocal tercer caracter
				// re = /\btr\w+\s?/gi; Palabras que empiezan por "tr".
				// re = /\btigres\b/gi; res += "str.replace = " + str.replace(re, "leones"); Cambiar tigres por leones.
				// re = /\s/gi; res += "str.replace = " + str.replace(re, ""); Eliminar todos los espacios.

				var res = "<h2>Probando... " + re.toString() + "</h2>";
				res += "re.exec = " + re.exec(str);
				res += "<br>re.test = " + re.test(str);
				res += "<br>str.search = " + str.search(re);
				var num_matches = (str.match(re) == null) ? 0 : str.match(re).length;
				res += "<h3>str.match [" + num_matches + "] = " + str.match(re) + "</h3>";
				res += "str.replace = " + str.replace(re, "*");
				document.getElementById("res").innerHTML = res;
		}
