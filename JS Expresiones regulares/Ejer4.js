document.addEventListener("DOMContentLoaded", function() {
	inputs = document.getElementsByTagName("input");
	var numInputs = inputs.length;
	var validar = document.getElementById("validar");
	validar.onclick = validacionGeneral;

	for (let i = 0; i < numInputs; i++) {
		inputs[i].addEventListener("blur", comprobante, false);
	}
}, false);

function comprobante() {
	switch (this.id) {
		case "dni":
			var re = /^\d{8}[a-zA-Z]$/i;
			var dni = this.value;
			estilo(re.test(dni), this);
			break;
		case "nombre":
			var re = /^(\b[a-zñA-ZÑ][ªº]|\b[a-zñA-ZÑ]+)\s?(\b[a-zñA-ZÑ])*$/;
			var nombre = this.value;
			estilo(re.test(nombre), this);
			break;
		case "apellidos":
			var re = /^(\b[a-zñA-ZÑ]+\s?)+$/i;
			var apellidos = this.value;
			estilo(re.test(apellidos), this);
			break;
		case "telefono":
			var re = /^(\+?\d{2,3})(\d{7,9})$/;
			var telefono = this.value;
			estilo(re.test(telefono), this);
			break;
		case "email":
			var re = /\w+@[a-zA-Z]+\.[a-zA-Z]{2,5}$/i;
			var email = this.value;
			estilo(re.test(email), this);
			break;

	}
}

function estilo(booleano, input) {
	if (booleano) {
		input.style.backgroundColor = '#00FF00';
		input.style.color = "blue";
	} else {
		input.style.backgroundColor = '#FF2B2B';
		input.style.color = "white";
	}
}

function validacionGeneral(){
	var azul = "blue"; //COLOR DE LETRA CUANDO ESTÁ BIEN.
	for (i=0; i<inputs.length; i++){
		if(inputs[i].style.color !== azul){
			estilo(false, inputs[i]);
		}
	}
}