window.onload = function () {
				document.getElementById("test").addEventListener("click", test, false);
				document.getElementById("str").addEventListener("keypress", function (e) {if(e.which==13) test();}, false);
				document.getElementById("re").addEventListener("keypress", function (e) {if(e.which==13) test();}, false);
			}


			function test() {
				var re = document.getElementById("re").value;
				var str = document.getElementById("str").value;
				//str = "Tres tristes tigres tragaban trigo en tres tristes trastos.";
				
				// re = new RegExp(re, "gi");
				// re = new RegExp("t\\w{3,}", "gi");
				// re = /t\w{3,}/gi;

				// re = /\d{4,5}/g; CODIGO POSTAL
				// re = /\d{4}[A-Z][^QÑR]{3}/gi; MATRICULAS
				// re = /[89](\d{8})/g; TELEFONOS FIJOS ESPAÑA
				// re = /(\+34)?[67]{1}(\d{8})/g; TELEFONOS MOVILES ESPAÑA
				// re = /(0[1-9]|[1-2]\d|3[01])(\-|\/)(0[1-9]|1[012])(\-|\/)(\d{4}|\d{2})/gi; FECHA

				
				var res = "<h2>Probando... " + re.toString() + "</h2>";
				res += "re.exec = " + re.exec(str);
				res += "<br>re.test = " + re.test(str);
				res += "<br>str.search = " + str.search(re);
				var num_matches = (str.match(re) == null) ? 0 : str.match(re).length;
				res += "<h3>str.match [" + num_matches + "] = " + str.match(re) + "</h3>";
				res += "str.replace = " + str.replace(re, "*");
				document.getElementById("res").innerHTML = res;
		}