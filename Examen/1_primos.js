window.onload = function() {
	document.getElementById("accion").setAttribute("onclick", "principal()");
}


function principal() {
	// Capturar datos

	// Procesar
	var res = getPrimos();

	// Mostrar resultados
	document.getElementById("resultado").innerHTML = res;
}


function getPrimos() {
	var res = new Array();
	for (let i = 5; i < 100; i += 2) {
		if (!esPrimo(i)) {
			continue;
		} else {
			console.log(i);
			res.push(i);
		}
	}
	return res;
}


function esPrimo(i) {
	var numDivisores = 0;
	var retorno = false;

	for(let k=1; k<=i; k++){	
		if (i%k == 0){
			numDivisores++;
			if(numDivisores > 2){
				break;
			}
		}
	}

	if(numDivisores>2){
		retorno = false;
	} else {
		retorno = true;
	}

	return retorno;
}