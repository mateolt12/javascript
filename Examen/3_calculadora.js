window.onload = function () {
	document.getElementById("btnSuma").setAttribute("onClick", "principal('suma')");
	document.getElementById("btnResta").setAttribute("onClick", "principal('resta')");
	document.getElementById("btnMult").setAttribute("onClick", "principal('mult')");
	document.getElementById("btnDiv").setAttribute("onClick", "principal('div')");
	document.getElementById("btnMod").setAttribute("onClick", "principal('mod')");
	document.getElementById("btnClear").setAttribute("onClick", "principal('c')");
}


function principal(tipo) {
	// Capturar datos
	var op1 = document.getElementById("op1").value;
	var op2 = document.getElementById("op2").value;
	
	// Procesar
	try{
	op1 = parsear(op1);
	op2 = parsear(op2);

	var res = operacion(tipo, op1, op2);
	
	// Mostrar resultados
	document.getElementById("resultado").innerHTML = res.toFixed(2);

	if(tipo=="c") {
		document.getElementById("op1").value = "";
		document.getElementById("op2").value = "";
	}
	} catch(err){
		document.getElementById("resultado").innerHTML = err;
	}
	
}


function operacion(tipo, op1, op2) {
	var num1 = op1;
	var num2 = op2;
	var res="";

	switch(tipo) {
		case "suma":
			res = num1+num2;
			break;
		case "resta":
			res = num1-num2;
			break;
		case "mult":
			res = num1*num2;
			break;
		case "div":
			res = num1/num2;
			break;
		case "mod":
			res = num1%num2;
			break;
	}
		
	return parseFloat(res);
}

function parsear(op){
	var op = parseFloat(op);
	if(isNaN(op)){
		throw "Err: operando no válido";
	} else{
		return op;
	}
}