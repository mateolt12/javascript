window.onload = function() {
	document.getElementById("accion").setAttribute("onclick", "principal()");
}


function principal() {
	// Capturar datos
	var a = document.getElementById("datos").value;

	// Procesar
	var res = esPalindromo(a);

	// Mostrar resultados
	document.getElementById("resultado").innerHTML = res;
}


function esPalindromo(texto) {
	var textoAlReves = "";
	var longitud = texto.length;

	while(longitud>=0){
		textoAlReves+=texto.charAt(longitud)
		longitud--;
	}

	if (textoAlReves == texto) {
		return true;
	} else

		return false;
}
