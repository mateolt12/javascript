var ventanaEmergente;
window.onload = function() {

	ventanaEmergente = mostrarVentanaEmergente();
	documentoVentanaEmergente = ventanaEmergente.document.open();

	// Capturar datos
	var operadores = capturarDatos(ventanaEmergente);

	// Procesar
	var resultadoProducto = 1;
	for(let j=0; j<operadores.length; j++){
		resultadoProducto*=operadores[j];
	}
	var tablaMultiplicarNum1 = "<table><tr><th>TABLA MULTIPLICAR</th></tr>";
	for (let k=0; k<=9; k++){
		tablaMultiplicarNum1+="<tr><td>"+operadores[0] + " X "+k+" = "+(operadores[0] * k)+"</td></tr>"
	}
	tablaMultiplicarNum1+="</table>";

	// Mostrar resultados
	document.getElementById("op1").innerHTML = operadores[0];
	document.getElementById("op2").innerHTML = operadores[1];
	document.getElementById("resultado").innerHTML = (operadores[0]*operadores[1]);
	documentoVentanaEmergente.write(tablaMultiplicarNum1);

	

}

function capturarDatos(ventanaEmergente) {
	var numerosUsuario = new Array();

	for (let i = 1; i <= 2; i++) {
		numerosUsuario.push(ventanaEmergente.prompt("Introduce el " + i + " º número."));

	}

	return numerosUsuario;
}

function mostrarVentanaEmergente() {
	ventanaEmergente = open("", "Emergente", "resizable=yes");
	return ventanaEmergente;
}
