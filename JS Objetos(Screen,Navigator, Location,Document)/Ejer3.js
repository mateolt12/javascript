window.onload = function() {
	document.getElementById("asignar").onclick = asignar;
	document.getElementById("recargar").onclick = recargar;
	document.getElementById("replace").onclick. = replace;
	document.getElementById("propiedades").onclick = propiedades;
}

function propiedades() {
	var has = location.hash = "#apartado";
	var hos = location.host;
	var hostn = location.hostname;
	var pagina = location.href;
	var orig = location.origin;
	var pat = location.pathname;
	var puerto = location.port;
	var pro = location.protocol;
	var se = location.search;
	//document.write("////Location.hash: " + has + "<br>");
	document.write("////Location.host: " + hos + "<br>");
	document.write("////Location.hostname: " + hostn + "<br>");
	document.write("////Location.href: " + pagina + "<br>");
	document.write("////Location.Origin: " + orig + "<br>");
	document.write("////Location.Pathname: " + pat + "<br>");
	document.write("////Location.port: " + puerto + "<br>");
	document.write("////Location.protocol: " + pro + "<br>");
	document.write("////Location.search: " + se + "<br>");
}

function recargar() {
	location.reload();
}

function asignar() {
	location.assign("https://www.google.com");
}

function replace() {
	location.replace("https://www.google.com");
}