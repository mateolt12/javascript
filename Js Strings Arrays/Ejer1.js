function suma() {
    operando1 = parseInt(document.getElementById("operando1").value.trim());
    operando2 = parseInt(document.getElementById("operando2").value.trim());

    if (isNaN(operando1) || isNaN(operando2)) {
        document.getElementById("resultado").value = "Error";
        document.getElementById("operacion").innerHTML = "Operador no válido"

    } else {
        document.getElementById("operacion").innerHTML = "";
        document.getElementById("resultado").value = (operando1 + operando2);
    }
}
function resta() {
    operando1 = parseInt(document.getElementById("operando1").value.trim());
    operando2 = parseInt(document.getElementById("operando2").value.trim());

    if (isNaN(operando1) || isNaN(operando2)) {
        document.getElementById("resultado").value = "Error";
        document.getElementById("operacion").innerHTML = "Operador no válido";

    } else {
        document.getElementById("operacion").innerHTML = "";
        document.getElementById("resultado").value = (operando1 - operando2);
    }
}

function multiplicacion() {
    operando1 = parseInt(document.getElementById("operando1").value.trim());
    operando2 = parseInt(document.getElementById("operando2").value.trim());

    if (isNaN(operando1) || isNaN(operando2)) {
        document.getElementById("resultado").value = "Error";
        document.getElementById("operacion").innerHTML = "Operador no válido"

    } else {
        document.getElementById("operacion").innerHTML = "";
        document.getElementById("resultado").value = (operando1 * operando2);
    }
}

function division() {
    operando1 = parseInt(document.getElementById("operando1").value.trim());
    operando2 = parseInt(document.getElementById("operando2").value.trim());

    if (isNaN(operando1) || isNaN(operando2)) {
        document.getElementById("resultado").value = "Error";
        document.getElementById("operacion").innerHTML = "Operador no válido"

    } else {
        document.getElementById("operacion").innerHTML = "";
        document.getElementById("resultado").value = (operando1 / operando2);
    }
}

function modulo() {
    operando1 = parseInt(document.getElementById("operando1").value.trim());
    operando2 = parseInt(document.getElementById("operando2").value.trim());

    if (isNaN(operando1) || isNaN(operando2)) {
        document.getElementById("resultado").value = "Error";
        document.getElementById("operacion").innerHTML = "Operador no válido"

    } else {
        document.getElementById("operacion").innerHTML = "";
        document.getElementById("resultado").value = (operando1 % operando2);
    }
}

function borrar() {
    document.getElementById("operando1").value = null;
    document.getElementById("operando2").value = null;
    document.getElementById("resultado").value = null;
    document.getElementById("operacion").innerHTML = null;

}