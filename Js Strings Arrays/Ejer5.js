window.onload = function () {
    do {
        var nif = parseInt(prompt("Introduce los 8 dígitos del DNI para calcular la letra correspondiente."));

        letrasDNI = "TRWAGMYFPDXBNJZSQVHLCKE";
        comprobacion = comprueba(nif);
    } while (comprobacion == false);

    var resto = nif % 23;
    var dni = nif + letrasDNI.charAt(resto);
    alert("Tu letra es: " + letrasDNI.charAt(resto));

    alert("Tu dni sería:  " + dni);

}

function comprueba(nif) {
    if (isNaN(nif) || nif.toString().length !== 8) {
        alert("Error: Has introducido un número erróneo.");
        return false;
    } else {
        return true;
    }

}