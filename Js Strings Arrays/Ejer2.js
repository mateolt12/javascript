window.onload = function(){
    numero = parseInt(prompt("Introduce el número a calcular."));
    
    while (isNaN(numero)){
        alert('Error: El valor introducido no es un número :(');
        numero = parseInt(prompt("Introduce el número a calcular."));
    }
    
    esPar(numero);
}

function esPar(numero){
    if(numero%2 == 0){
        document.getElementById("numero").innerHTML = numero + " es un número par.";
    } else{
        document.getElementById("numero").innerHTML = numero + " es un número impar.";
    }
}

