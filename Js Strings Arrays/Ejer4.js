window.onload = function () {

}

function creaLista() {

    arrayHorasInter=[];
    contador=0;
  
    horaInicio = comprueba(document.getElementById("inicio").value);
    horaFin = comprueba(document.getElementById("fin").value);
  
    limpiaSelect();
  
  
  for(let i=horaInicio; i<=horaFin; i++){
      for(let j=0; j<6; j++){
          arrayHorasInter[contador] = "<option>" + i +":"+ (j*10)+"</option>";
          contador++;
      }
  }
  
  for(let i=0; i<arrayHorasInter.length; i++){
      document.getElementById("selector").innerHTML+=arrayHorasInter[i];
  }
  
}

function comprueba(numero){
    if (isNaN(parseInt(numero)) || parseInt(numero)<0 || parseInt(numero)>23) {
        alert("Error: Alguno de los valores introducidos es incorrecto :(");
    }else{
        return numero;
    }

}

function limpiaSelect(){
    document.getElementById("selector").options.length = 0;
}