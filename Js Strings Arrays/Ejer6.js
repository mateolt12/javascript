window.onload = function () {

}

function comprueba(texto) {

    if (isNaN(texto)) {
        return true;
    } else {

        return false;
    }

}

function darLaVuelta(texto) {
    texto = texto.replace(/ /g,"").toLowerCase();
    var i = texto.length;
    var textoReves = "";

    while (i >= 0) {
        textoReves = textoReves + texto.charAt(i);
        i--;
    }
    return textoReves;
}

function mensajeFinal() {
    var texto = document.getElementById("texto").value;

    if (comprueba(texto) == false) {
        alert("Error, no es una cadena de texto. :(");
    } else {

        var textoReves = darLaVuelta(texto);

        if (textoReves === texto.replace(/ /g,"").toLowerCase()) {
            alert("Si es palíndromo.");
        } else {
            alert("No es palíndromo.");
        }
    }

}

function borrar() {
    document.getElementById("texto").value = null;
}
