window.onload = function(){
    numero = parseInt(prompt("Introduce el número a calcular su factorial."));
    
    while (isNaN(numero)){
        alert('Error: El valor introducido no es un número :(');
        numero = parseInt(prompt("Introduce el número a calcular su factorial."));
    }
    
    document.getElementById("numero").innerHTML = "El factorial de "+numero+" es: "+factorial(numero);
}

function factorial(numero){
    result = numero;
    for(let i=(numero-1); i>0; i--){
        result*=i;
    }
    return result;
}

