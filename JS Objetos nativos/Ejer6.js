window.onload = function () {
    document.getElementById("comprobar").onclick = calcular;
    document.getElementById("navidad").onclick = navidad;
}

function validarFecha(dia, mes) {

    if (dia < 1 || dia > 31 || mes < 1 || mes > 12) {
        return false;
    } else {
        return true;
    }


}

function calcular() {

    var diasSemana = ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"];
    var dia = parseInt(document.getElementById("dia").value);
    var mes = parseInt(document.getElementById("mes").value);
    var ano = parseInt(document.getElementById("ano").value);

    try {
        if (isNaN(dia) || isNaN(mes) || isNaN(ano) || !validarFecha(dia, mes)) {
            throw new Error();

        } else {
            var data = new Date(ano, mes - 1, dia);
            var diaNombre = diasSemana[data.getDay()];
            document.getElementById("visualizacion").innerHTML = diaNombre;
        }

    } catch (err) {
        document.getElementById("visualizacion").innerHTML = "Has introducido algun parámetro no válido."
    }

}

function navidad() {
    var dia = parseInt(document.getElementById("dia").value);
    var mes = parseInt(document.getElementById("mes").value);
    var ano = parseInt(document.getElementById("ano").value);

    try {
        if (isNaN(dia) || isNaN(mes) || isNaN(ano) || !validarFecha(dia, mes)) {
            throw new Error();

        } else {
            var fechaHoy = new Date();
            var data = new Date(ano, mes - 1, dia, fechaHoy.getHours(), fechaHoy.getMinutes());
            var navidad = new Date(2018, 11, 25);

            var diferencia = navidad.getTime() - data.getTime();
            
            var diasRestantes = diferencia / 86400000;
            var restoCalculo = diferencia % 86400000;
            var horas = restoCalculo / 3600000;
            restoCalculo = restoCalculo % 3600000;
            var minutos = restoCalculo / 60000;

            document.getElementById("visualizacion").innerHTML = "Faltan " + Math.round(diasRestantes) + " días, " + Math.round(horas) + " horas, " + Math.round(minutos) + " minutos para Navidad.";

        }

    } catch (err) {
        document.getElementById("visualizacion").innerHTML = "Has introducido algun parámetro no válido."
    }
}