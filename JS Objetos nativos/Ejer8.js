window.onload = function () {
    document.getElementById("comprobar").onclick = calcular;

}

function validarFecha(dia, mes, diaSemana) {

    var diasSemana = ["domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sabado"];
    var meses = ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto",
        "septiembre", "octubre", "noviembre", "diciembre"];

    if (!diasSemana.includes(diaSemana)) {
        return false;
    }

    if (!meses.includes(mes)) {
        return false;
    }

    if (dia < 1 || dia > 31) {
        return false;
    } else {
        return true;
    }
}

function borrar(){
    document.getElementById("visualizacion").innerHTML = null;
}



function calcular() {
    borrar();
    var diasSemana = ["domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sabado"];
    var meses = ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto",
        "septiembre", "octubre", "noviembre", "diciembre"];

    var dia = document.getElementById("dia").value;
    var mes = document.getElementById("mes").value;
    var diaSemana = document.getElementById("diaNombre").value;

    try {
        if (!validarFecha(dia, mes, diaSemana)) {
            throw new Error();
        } else {
            for (let i = 2000; i <= 2050; i++) {
                var fecha = new Date(i, meses.indexOf(mes), dia);
                var diaDeSemana = parseInt(fecha.getDay());
                if(diaSemana == diasSemana[diaDeSemana]){
                    document.getElementById("visualizacion").innerHTML += fecha.getFullYear() + "<br>";
                }
                
            }

        }

    } catch (err) {
        document.getElementById("visualizacion").innerHTML = "Has introducido un o varios valores erróneos";
    }

}

