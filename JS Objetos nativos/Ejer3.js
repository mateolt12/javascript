window.onload = function () {
    document.getElementById("visualizacion").innerHTML = fechaActual();
}
function fechaActual() {
    var meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto",
        "Septiembre", "Octubre", "Noviembre", "Diciembre"];
    var diasSemana = ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"];

    var data = new Date();
    var dia = data.getDate();
    var diaNombre = diasSemana[data.getDay()];
    var mes = meses[data.getMonth()];
    var año = data.getFullYear();
    var hora = data.getHours();
    var minutos = data.getMinutes();
    

    return "Hoy es " + diaNombre + ", " + dia + " de " + mes + " de " + año + " y son las " + hora + ":" + minutos + " horas.";
}


