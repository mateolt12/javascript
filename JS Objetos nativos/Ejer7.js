window.onload = function () {
    document.getElementById("comprobar").onclick = calcular;

}

function validarFecha(dia, mes) {

    if (dia < 1 || dia > 31 || mes < 1 || mes > 12) {
        return false;
    } else {
        return true;
    }
}

function calcular() {
    var dia = parseInt(document.getElementById("dia").value);
    var mes = parseInt(document.getElementById("mes").value);
    var ano = parseInt(document.getElementById("ano").value);

    try {
        if (isNaN(dia) || isNaN(mes) || isNaN(ano) || !validarFecha(dia, mes)) {
            throw new Error();

        } else {
            var dataLimite = new Date(ano, mes - 1, (dia + 15));

            document.getElementById("visualizacion").innerHTML = "Has de devolver el libro antes del: "
                    + dataLimite.getDate() + "/" + (dataLimite.getMonth() + 1) + "/" + dataLimite.getFullYear();
        }

    } catch (err) {
        document.getElementById("visualizacion").innerHTML = "Has introducido algun parámetro no válido."
    }
}
