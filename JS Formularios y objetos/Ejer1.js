function añadirFruta() { //se llama cuando el usuario hace un clic sobre la caja de texto o sobre la etiqueta asociada a la caja
	//selecciona el contenido de la caja de texto
	document.getElementById("nuevaFruta").select();
	//activa directamente el botón de Añadir Fruta
	document.getElementById("añadir").checked = true;
}

function seleccionar() { //se llama cuando el usuario hace un clic sobre la lista de frutas o sobre la etiqueta asociada a la lista
	//activa directamente el botón de Borrar frutas seleccionadas
	document.getElementById("borrar").checked = true;
}

function repetida() {
	var frutas = document.getElementById("misFrutas");
	var nuevaFruta = document.getElementById("nuevaFruta").value;
	//comprobamos si existe en la lista la fruta a añdir
	var i = 0;
	while (i < frutas.length) {
		if (frutas[i].value.toLowerCase() == nuevaFruta.toLowerCase()) {
			return true;
		}
		i++;
	}
	return false;
}

function añadir() {
	var nuevaFruta = document.getElementById("nuevaFruta").value;
	if (nuevaFruta == "") {
		alert("Se ha dejado en blanco el nombre de la fruta");
	} else if (repetida()) {
		alert("Has introducido una fruta repetida.");
	} else {
		var frutas = document.getElementById("misFrutas");
		var opcion = document.createElement("option");
		opcion.text = nuevaFruta.charAt(0).toUpperCase().concat(nuevaFruta.substring(1).toLowerCase());

		if (opcion.text > frutas[frutas.length - 1].value) {
			frutas.add(opcion);
		} else {
			for (let i = 0; i < frutas.length; i++) {
				if (opcion.text < frutas[i].value) {
					frutas.add(opcion, i);
					opcion.value = opcion.text;
					break;
				}
			}
		}

	}
}

function seleccionado() {
	var frutas = document.getElementById("misFrutas");
	//comprobamos que existe algún elemento seleccionado
	var i = 0;
	while (i < frutas.length) {
		if (frutas[i].selected) {
			return true;
		}
		i++;
	}
	return false;
}

function borrar() {
	var frutas = document.getElementById("misFrutas");
	if (seleccionado()) {
		if (confirm("los elementos seleccionados van a ser borrados ¿está seguro de que desea borrarlos?")) { //recorremos los elementos de la lista
			for (let i = frutas.length - 1; i >= 0; i--) {
				frutas.remove(frutas.selectedIndex);
			}
		}
	} else {
		alert("No se ha seleccionado ningún elemento");
	}
}

function operar() { //realiza la operación de añadir un elemento a la lista o de borrar los elementos de la lista que estén seleccionados
	//comprobamos que botón de opción está seleccionado
	var seleccion = document.getElementsByTagName("operacion");
	if (document.frutas.añadir.checked) { //está seleccionado añadir
		añadir();
	} else {
		if (document.frutas.borrar.checked) { //está seleccionado borrar
			borrar();
		} else { //no está seleccionada ninguna de las opciones
			alert("Debes seleccionar una de las dos opciones");
		}
	}
}