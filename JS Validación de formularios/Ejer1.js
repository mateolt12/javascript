document.addEventListener("DOMContentLoaded", function() {
	var inputs = document.getElementsByTagName("input");
	for (let i = 0; i < inputs.length; i++) {

		switch (inputs[i].id) {
			case "dni":
				inputs[i].addEventListener("invalid", validaDNI, false);
				inputs[i].addEventListener("input", validarTodos, false);
				break;

			case "nombre":
				inputs[i].addEventListener("invalid", validaNombre, false);
				inputs[i].addEventListener("input", validarTodos, false);
				break;

			case "apellidos":
				inputs[i].addEventListener("invalid", validaApellidos, false);
				inputs[i].addEventListener("input", validarTodos, false);
				break;

			case "telefono":
				inputs[i].addEventListener("invalid", validaTelefono, false);
				inputs[i].addEventListener("input", validarTodos, false);
				break;

			case "email":
				inputs[i].addEventListener("invalid", validaEmail, false);
				inputs[i].addEventListener("input", validarTodos, false);
				break;
		}
	}

}, false);


function validarTodos() {
	this.classList.remove("invalido");
	event.target.setCustomValidity("");
}

function validaDNI() {
	if (this.validity.valueMissing) {
		event.target.setCustomValidity("Debe introducir un DNI, no puede estar vacío.");
		this.classList.add("invalido");
	} else if (!this.validity.valid) {
		event.target.setCustomValidity("DNI no válido. Cambie su DNI por otro válido.");
		this.classList.add("invalido");
	}
}

function validaNombre() {
	if (this.validity.valueMissing) {
		this.classList.add("invalido");
		event.target.setCustomValidity("Debe introducir un nombre, no puede estar vacío.");
	} else if (!this.validity.valid) {
		this.classList.add("invalido");
		event.target.setCustomValidity("Nombre no válido. Cambie su nombre por otro válido.");
	}
}

function validaApellidos() {
	if (this.validity.valueMissing) {
		this.classList.add("invalido");
		event.target.setCustomValidity("Debe introducir sus apellidos, no puede estar vacío.");
	} else if (!this.validity.valid) {
		this.classList.add("invalido");
		event.target.setCustomValidity("Apellidos no válidos. Cambie sus apellidos por otros válidos.");
	}
}

function validaTelefono() {
	if (!this.validity.valid) {
		this.classList.add("invalido");
		event.target.setCustomValidity("Debe introducir un teléfono, no puede estar vacío.");
	} else if (this.validity.rangeOverflow) {
		this.classList.add("invalido");
		event.target.setCustomValidity("El teléfono debe contener entre 7 y 9 números");
	} else if (this.validity.valueMissing) {
		this.classList.add("invalido");
		event.target.setCustomValidity("Teléfono no válido. Cambie su teléfono por otro válido.");
	}


}

function validaEmail() {
	if (this.validity.valueMissing) {
		this.classList.add("invalido");
		event.target.setCustomValidity("Debe introducir un email, no puede estar vacío.");
	} else if (!this.validity.valid) {
		this.classList.add("invalido");
		event.target.setCustomValidity("Email no válido. Cambie su email por otro válido.");
	}
}