var w3c = document.getElementById("w3c");
var contadorClick = 0;

w3c.addEventListener("click", contarClicks, true);

function contarClicks() {

	this.innerHTML = "modelo del W3C - Nº de clicks: " + (++contadorClick);

}

w3c.addEventListener("mouseover", cambiarEstilo, false);
w3c.addEventListener("mouseout", quitarEstilo, false);

function cambiarEstilo() {
	this.style.backgroundColor = 'yellow';
	this.style.color = 'red'

}

function quitarEstilo() {
	this.style.backgroundColor = 'transparent';
	this.style.color = 'initial';
}


var w3canonima = document.getElementById("w3canonima");

w3canonima.addEventListener("mousedown", function() {
	if (this.style.backgroundColor == "black") {
		this.style.backgroundColor = "blue";
	} else {
		this.style.backgroundColor = "black";
	}
}, false);