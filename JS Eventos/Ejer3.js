var w3c = document.getElementById("w3c");
var w3canonima = document.getElementById("w3canonima");
var contadorClick = 0;

function contarClicks() {

	this.innerHTML = "modelo del W3C - Nº de clicks: " + (++contadorClick);

}

function cambiarEstilo() {
	this.style.backgroundColor = 'yellow';
	this.style.color = 'red'

}

function quitarEstilo() {
	this.style.backgroundColor = 'transparent';
	this.style.color = 'initial';
}

if (this.addEventListener) {

	w3c.addEventListener("click", contarClicks, true);
	w3c.addEventListener("mouseover", cambiarEstilo, false);
	w3c.addEventListener("mouseout", quitarEstilo, false);

	w3canonima.addEventListener("mousedown", function() {
		if (this.style.backgroundColor == "black") {
			this.style.backgroundColor = "blue";
		} else {
			this.style.backgroundColor = "black";
		}
	}, false);

} else { //Antiguas versiones de IExplorer.
	w3c.attachEvent("onclick", contarClicks);
	w3c.attachEvent("onmouseover", cambiarEstilo);
	w3c.attachEvent("onmouseout", quitarEstilo);

	w3canonima.attachEvent("onmousedown", function() {
		if (this.style.backgroundColor == "black") {
			this.style.backgroundColor = "blue";
		} else {
			this.style.backgroundColor = "black";
		}
	});
}