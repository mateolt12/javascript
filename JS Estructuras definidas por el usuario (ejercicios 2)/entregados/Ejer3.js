var arrayPeliculas = new Array();

window.onload = function(){
	document.getElementById("almacenar").onclick = recogerValores;
	document.getElementById("mostrar").onclick = mostrarInfo;
}

function Pelicula() {

	this.nombre;
	this.director;
	this.ano;
	this.genero = new Array();
	this.clasificacion;
	this.localizacion;
	this.prestada;

	this.crearConParametros = function(nombre, director, ano, genero, clasificacion) {
		this.nombre = nombre;
		this.director = director;
		this.ano = ano;
		this.genero = this.genero.concat(genero);
		this.clasificacion = clasificacion;
		this.prestada = false;
		
	};

	this.crearSinParametros = function(){
		this.nombre = "";
		this.director = "";
		this.ano = "";
		this.genero = new Array();
		this.localizacion = 0;
		this.prestada = false;
	};

	this.setLocalizacion = function(nuevaLocalizacion){ //Método que permite cambiar el numero de la estantería en la localización.
		this.localizacion = nuevaLocalizacion;
	};

	this.setPrestada = function(valor){ //Método que permite cambiar el valor de prestada.
		this.prestada = valor;
	};

	this.mostrarInformacion = function(){
		var info = "Nombre: "+this.nombre+"<br>Director: "+this.director+"<br>Año: "+this.ano+"<br>"+
		"Género: "+this.genero+"<br>Clasificación: "+this.clasificacion+"<br>Localizacion: "+this.localizacion+"<br>"+
		"Prestada: "+this.prestada;
		return info;
	};
}

function recogerValores(){
	var genero = new Array();
	var nombre = document.getElementById("nombre").value;
	var director = document.getElementById("director").value;
	var ano = parseInt(document.getElementById("ano").value);
	var clasificacion = document.getElementById("clasificacion").value;
	var localizacion = parseInt(document.getElementById("localizacion").value);
	var prestada = document.getElementById("prestada").value;
	var generoAux = document.getElementById("genero"); 
	for(let i=0; i<generoAux.options.length; i++){
		let opt = generoAux.options[i];
		if(opt.selected){
			genero.push(opt.value);
		}
	}

	if(nombre!="" && director!="" && ano!="" && genero!="" && clasificacion!=""){
		var miPelicula = new Pelicula();
		miPelicula.crearConParametros(nombre, director, ano, genero, clasificacion);
		miPelicula.setLocalizacion(localizacion);
		if(prestada=="si"){
			miPelicula.setPrestada(true);
		}
		arrayPeliculas.push(miPelicula);
	}else{
		var miPelicula = new Pelicula();
		miPelicula.crearSinParametros();
		arrayPeliculas.push(miPelicula);
	}
	console.log(arrayPeliculas);
}

function mostrarInfo(){
	limpiarCampoInfo();
	for(let i in arrayPeliculas){
		document.getElementById("info").innerHTML += arrayPeliculas[i].mostrarInformacion()+"<br>";
		document.getElementById("info").innerHTML += "<hr>";
	}


}

function limpiarCampoInfo(){
	document.getElementById("info").innerHTML = "";
}
