var miPila = new Pila();
window.onload = function() {
	miPila.crear();
}

function Pila() {
	var elementos;
	this.crear = function() {
		this.elementos = new Array();
	}
	this.esVacia = function() {
		return (this.elementos.length == 0) ? true : false;
	}
	this.anadir = function(ele) {
		if (ele == "") {
			throw "Non se pode engadir un campo baleiro.";
		}
		this.elementos.push(ele);
		console.log(this.elementos);
	}
	this.extraer = function() {
		if (this.esVacia()) {
			throw "Non se pode extraer dunha pila baleira.";
		} else {
			return this.elementos.pop();
		}


	}
	this.listar = function() {
		var lista = "";
		if (this.esVacia()) {
			throw "Non se pode listar unha pila baleira."; //Throw termina el script No ejecuta lo que hay a continuación.
		}
		while (!this.esVacia()) {
			lista += this.extraer() + "<br>";
		}

		return lista;
	}
}

function anadeElemento() {
	try {
		limpiar();
		var elemento = document.getElementById("texto").value;
		miPila.anadir(elemento);
		limpiarInputText();
	} catch (err) {
		document.getElementById("mensaje").innerHTML = err;
	}

}

function extraeElemento() {
	try {
		limpiar();
		limpiarInputText();
		var resultado = miPila.extraer();
	} catch (err) {
		resultado = err;
	}
	document.getElementById("mensaje").innerHTML = resultado;
}

function listaElementos() {
	try {
		limpiar();
		limpiarInputText();
		var resultado = miPila.listar();
	} catch (err) {
		resultado = err;
	}
	document.getElementById("mensaje").innerHTML = resultado;
}

function limpiar() {
	document.getElementById("mensaje").innerHTML = "";
}

function limpiarInputText(){
	document.getElementById("texto").value = "";
}