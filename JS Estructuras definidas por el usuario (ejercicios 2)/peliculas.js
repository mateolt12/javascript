var peliculas = new Array();
window.onload = function() {
    document.getElementById("crear").setAttribute("onclick", "principal('crear')");
    document.getElementById("listar").setAttribute("onclick", "principal('listar')");
    document.getElementById("listarTodas").setAttribute("onclick", "principal('listarTodas')");
}
 
function principal(opcion) {
    switch (opcion) {
        case "crear":
            try {
                pelicula = new Pelicula();
                pelicula.crear(recuperarDatos()[0], recuperarDatos()[1], recuperarDatos()[2], recuperarDatos()[3],
                    recuperarDatos()[4], recuperarDatos()[5], recuperarDatos()[6]);
                peliculas.push(pelicula);
            } catch (e) {
                alert(e);
            }
            break;
        case "listar":
            document.getElementById("resultado").innerHTML = "**** Ultima pelicula añadida **** <br>" + pelicula.mostrar();
            break;
        case "listarTodas":
            document.getElementById("resultado").innerHTML = recuperarPeliculas();
            break;
    }
}
 
function recuperarPeliculas() {
    var tabla = "<table class='table table-striped'>";
    for (let i in peliculas) {
        tabla += "<tr><th colspan='7'>Pelicula " + (i+1)+ "</th></tr>";
        tabla += "<tr><td>" + peliculas[i].mostrar() + "</td></tr>";
    }
    tabla += "</table>";
    return tabla;
}
 
function recuperarDatos() {
    var nombre = document.getElementById("nombre").value;
    var director = document.getElementById("director").value;
    var ano = document.getElementById("ano").value;
    if (isNaN(ano)) throw "Solo se admiten numeros en la fecha";
    var genero = document.getElementById("genero").value;
    var clasificacion = document.getElementById("clasificacion").value;
    var localizacion = document.getElementById("localizacion").value;
    if (isNaN(localizacion)) throw "Solo se admiten numeros en la localizacion";
    var prestamo = recuperarRadio("prestamo");
    if(prestamo == "si"){
        pelicula.cambiarPrestamo(true);
    }else{
        pelicula.cambiarPrestamo(false);
    }  
    pelicula.cambiarNumEstanteria(localizacion);
    var datos = [nombre, director, ano, genero, clasificacion, localizacion, prestamo];
    return datos;
}
 
function recuperarRadio(opcion) {
    var valores = document.getElementsByName(opcion);
    var seleccionado;
    for (let i = 0; i < valores.length; i++) {
        if (valores[i].checked) {
            seleccionado = valores[i].value;
        }
    }
    return seleccionado;
}
 
function Pelicula() {
    this.nombre;
    this.director;
    this.ano;
    this.genero;
    this.clasificacion;
    this.localizacion;
    this.enPrestamo = false;
 
    this.crear = function(nombre, director, ano, genero, clasificacion, localizacion, enPrestamo) {
        this.nombre = nombre || "";
        this.director = director || "";
        this.ano = ano || "";
        this.genero = genero || "";
        this.clasificacion = clasificacion || "";
        this.localizacion = 0 || localizacion;
        this.enPrestamo = false || enPrestamo;
    }
 
    this.cambiarNumEstanteria = function(localizacionNueva) {
        this.localizacion = localizacionNueva;
    }
 
    this.cambiarPrestamo = function(valor) {
        this.enPrestamo = valor;
       
    }
 
    this.mostrar = function() {
        return "Nombre: " + this.nombre + "<br>" + "Director: " + this.director + "<br>" +
            "Año: " + this.ano + "<br>" + "Genero: " + this.genero + "<br>" +
            "Clasificacion: " + this.clasificacion + "<br>" + "Localizacion: " + this.localizacion + "<br>" +
            "Prestada: " + this.enPrestamo;
    }
 
}