window.onload = function() {
	var texto = document.getElementById("texto");
	var visualizacion = document.getElementById("visualizacion");

	texto.addEventListener("keypress", function(e) {

		if (e.which == 13) {
			
			visualizacion.innerHTML = texto.value;
			visualizacion.style.backgroundColor = 'lightblue';
		}

	}, false);

}