window.onload = function() {
	var div = document.getElementById("div");
	var resultado = document.getElementById("resultado");
	var titulo = document.getElementById("evento");


	div.addEventListener("mousemove", function(e) {
		var x = e.clientX;
		var y = e.clientY;
		div.style.backgroundColor = "#C0C0C0";
		titulo.innerHTML = "<h1>Ratón</h1>";
		resultado.innerHTML = x + "," + y;
	}, false);

	this.addEventListener("click", function(e) {
		var x = e.clientX;
		var y = e.clientY;
		div.style.backgroundColor = "#FCB187";
		titulo.innerHTML = "<h1>Ratón</h1>";
		resultado.innerHTML = x + "," + y;
	}, false);

	this.addEventListener("keypress", function(e) {
		var codigo = e.which;
		var caracter = e.key;
		div.style.backgroundColor = "#686A96";
		titulo.innerHTML = "<h1>Teclado</h1>";
		resultado.innerHTML = "Código: "+codigo+"<br>Caracter: "+caracter;

	}, false)

}
