document.addEventListener('DOMContentLoaded', function() {
	formulario = document.forms[0];
	inputs = formulario.getElementsByTagName("input");
	numInputs = inputs.length;
	  
	for (let i = 0; i < numInputs; i++) {
		inputs[i].addEventListener("focus", resaltar, true);
		inputs[i].addEventListener("blur", quitarResaltado, true);
		inputs[i].addEventListener("keydown", moverse, true);
	}

}, true);

function resaltar() {
	this.style.border = "3px solid blue";
}

function quitarResaltado() {
	this.style.border = null;
}

function moverse(event) {
	var posicion = getIndex(this);
	if (event.which == 13) {
		if (posicion == (numInputs - 1)) {
			inputs[0].focus();
		} else {
			inputs[posicion + 1].focus();
		}
	}

	if (event.which == 40) {
		if (posicion != (numInputs - 1)) {
			inputs[posicion + 1].focus();
		}
	}

	if (event.which == 38) {
		if (posicion != 0) {
			inputs[posicion - 1].focus();
		}
	}
}

function getIndex(input) {
	for (let i = 0; i < numInputs; i++) {
		if (input === inputs[i]) {
			return i;
		}
	}
}