document.addEventListener('DOMContentLoaded', function() {
	var texto = document.getElementById("texto");
	barra = document.getElementById("progress");
	var avisoStop = lanzarPregunta();
	texto.setAttribute("maxlength", avisoStop);
	barra.max = avisoStop;
	texto.value.length = 0;
	var contador = avisoStop;
	document.getElementById("contador").innerHTML = "Le quedan " + contador + " caracteres."

	texto.addEventListener("keypress", function() {
		while (contador > 0) {
			contador--;
			barra.value++;
			document.getElementById("contador").innerHTML = "Le quedan " + contador + " caracteres."
			break;
		}
		if (texto.value.length == avisoStop) {
			lanzarAviso();
		}
	}, false);

	texto.addEventListener("keydown", function(e) {
		if (e.which == 8) {
			while(contador<avisoStop){
			barra.value--;
			contador++;
			document.getElementById("contador").innerHTML = "Le quedan " + contador + " caracteres."
			break;
			}
			
		}
	}, true);

}, true);

function lanzarPregunta() {
	var pregunta = window.prompt("Introduce el número máximo de caracteres que quieres almacenar en el area de texto.");
	return pregunta;
}


function lanzarAviso() {
	window.alert("Has alcanzado el limite de caracteres permitidos.");
}