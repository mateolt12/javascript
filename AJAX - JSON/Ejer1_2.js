$(document).ready(function() {
	$("#txt1").keyup(showHint)
});

function showHint() {
	var str = this.value;
	var url = "hint.php?q=" + str;
	if (str.length == 0) {
		$("#txtHint").text("");
		return;
	}
	$.get(url, function(data, status) {
		$("#txtHint").text(data);
	});
}