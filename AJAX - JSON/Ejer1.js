document.addEventListener("DOMContentLoaded", function(){
document.getElementById("txt1").addEventListener("keyup", showHint, false);

}, false);

function showHint(){
    var str = this.value;
  var url = "getHints.php?q="+str;
  if (str.length == 0) { 
    $("#txtHint").text("");
        return;
  }
  $.get(url, function(data, status){
    $("#txtHint").text(data);
  });
}