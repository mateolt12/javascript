<?php 
$modulos = array();

$modulos[0] = "DWCS";
$modulos[1] = "DWCC";
$modulos[2] = "DDI";
$modulos[3] = "DDA";
$modulos[4] = "EIEM";

$q = $_REQUEST["q"];
$hint = "";

if ($q !== "") {
    $q = strtolower($q);
    $len=strlen($q);
    foreach($modulos as $name) {
        if (stristr($q, substr($name, 0, $len))) {
            if ($hint === "") {
                $hint = $name;
            } else {
                $hint .= ", $name";
            }
        }
    }
}

echo $hint === "" ? "sin sugerencia" : $hint;

?>
