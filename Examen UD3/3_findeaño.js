window.onload = function() {
	document.getElementById("accion").onclick = principal;
}


function principal() {
	// Procesar
	var finano = crearFinAno();
	try{
	var validado = esDiaSem(finano, "lunes");
	var res = getTexto(finano);

	// Mostrar resultados
	document.getElementById("resultado").innerHTML = res;
} catch(err){
	document.getElementById().innerHTML = err.message;
}

}


// Devuelve un objeto Date con el fin de año aleatorio del presente siglo
function crearFinAno() {
	var fechaFin = new Date(2100, 11, 31);
	var anoAleatorio = Math.round(Math.random()*(2100-2018) + 2018);
	var finAnoAleatorio = new Date(anoAleatorio, 11, 31);

	return finAnoAleatorio;

}


// Comprueba si el día de la semana de la fecha coincide con el proporcionado
function esDiaSem(fecha, diaSem) {
	var diasSemana = new Array("domingo", "lunes", "martes", "miercoles", "jueves", "viernes", "sabado");
	var comprobante = false;
	if (diasSemana.includes(diaSem)) {
		if (fecha.getDay() == diasSemana.indexOf(diaSem)) {
			comprobante = true;
		}
	} else {
		throw "No se ha introducido un día correcto.";
	}

	return comprobante;
}


// Formatea el objeto Date en el formato de fecha largo en español
function getTexto(fecha) {
	var diasSemana = new Array("domingo", "lunes", "martes", "miercoles", "jueves", "viernes", "sabado");
	var mes = new Array("enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre");
	var texto = "<span style='color:";

	switch (fecha.getDay()) {
		case 0:
			texto += "red'>"+diasSemana[0];
			break;
		case 1:
			texto += "green'>"+diasSemana[1];
			break;
		case 2:
			texto += "red'>"+diasSemana[2];
			break;
		case 3:
			texto += "red'>"+diasSemana[3];
			break;
		case 4:
			texto += "red'>"+diasSemana[4];
			break;
		case 5:
			texto += "red'>"+diasSemana[5];
			break;
		case 6:
			texto += "red'>"+diasSemana[6];
			break;
	}

	texto += ", "+fecha.getDate()+" de ";

	for (let i = 0; i<= mes.length; i++) {
		if(i==fecha.getMonth()){
			var mesCorrecto = mes[i];
			break;
		}
	}

	texto+=mesCorrecto+" de "+fecha.getFullYear()+"</span>";
	return texto;
}

function cambiaColor(fecha){
	if(fecha.getDay()==1){

	}
}