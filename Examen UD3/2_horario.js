window.onload = function() {
	document.getElementById("accion").onclick = principal;
}


function principal() {
	// Procesar
	var array_horas = generarHorario();

	// Mostrar resultados
	res = crearSelect(array_horas);
	document.getElementById("resultado").innerHTML = res;

	crearSelect(array_horas);
}


function generarHorario() {
	var diaActual = new Date();
	var horaActual = diaActual.getTime();

	var diaFin = new Date();
	diaFin.setFullYear(diaActual.getFullYear(), diaActual.getMonth(), (diaActual.getDate() + 1));
	diaFin.setTime(diaActual.getTime() + 86400000);
	var horaFin = diaFin.getTime();

	var contador = horaActual;
	var horas = new Array();

	while (contador <= horaFin) {
		horas.push(contador);
		contador += 6000000;
	}

	var arrayHoras = new Array();

	for (i in horas) {
		var diaAux = new Date(horas[i]);
		arrayHoras.push(diaAux.toLocaleTimeString());
	}
	arrayHoras.push(diaFin.toLocaleTimeString());

	return arrayHoras;

}


function crearSelect(horas) {
	var selector = "<select id='selectorHoras'>"
	for (i in horas) {
		selector += "<option>" + horas[i] + "</option>";
	}
	selector += "</select>";
	return selector;
}