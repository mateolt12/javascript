var ventana;
var temporizador;


window.onload = function() {
	document.getElementById("lanzar").setAttribute("onclick", "lanzar()");
	document.getElementById("cerrar").disabled = true;
	document.getElementById("cerrar").setAttribute("onclick", "cerrar()");
}

function lanzar() {
	temporizador=setTimeout(escribirEnNuevaVentana, 2000);
}

function cerrar() {

	if(ventana){
		ventana.close();
		window.close();
	} else{
		alert("No existe ventana emergente");
	}

}

function nuevaVentana() {

	var y = window.innerHeight;
	var x = window.innerWidth;
	ventana = window.open("", "", "width=400, height = 220");
	ventana.moveTo(x / 2, y / 2);
	ventana.focus();
	return ventana;
}

function escribirEnNuevaVentana() {
	var ventanaN = nuevaVentana();
	var x = ventanaN.innerWidth;
	var y = ventanaN.innerHeight;

	var tabla = "<h2>SUBVENTANA</h2><table style='border: solid black 1px; text-align:center'><tr><td><h3>" + x + "</h3></td></tr><tr><td><h3>" + y + "</h3></td></tr>";
	tabla += "<tr><td><button id='desbloquear'>Desbloquear</td></tr></table>";
	ventana.document.write(tabla);
	ventana.document.getElementById("desbloquear").onclick = function() {
		ventana.opener.document.getElementById("cerrar").disabled = false;
	}

}