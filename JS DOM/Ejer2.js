document.addEventListener("DOMContentLoaded", function() {
	document.getElementById("anadir").addEventListener("click", anadirElemento, false);

});

function anadirElemento() {
	var lista = document.getElementById("lista");
	var nuevo = document.getElementById("texto").value;
	var texto = document.createTextNode(nuevo);
	var li = document.createElement("li");
	li.appendChild(texto);

	for (let i = 0; i < lista.children.length; i++) {
		if (texto.nodeValue.toLowerCase() < lista.children[i].childNodes[0].nodeValue.toLowerCase()) {
			lista.insertBefore(li, lista.children[i]);
			break;
		} else{
			lista.appendChild(li);
		}

	}
}