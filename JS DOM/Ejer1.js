document.addEventListener("DOMContentLoaded", function() {

	document.getElementById("crearparrafo").addEventListener("click", crearParrafo, false);
	document.getElementById("crearimagen").addEventListener("click", crearImagen, false);
	document.getElementById("borrarultimo").addEventListener("click", borrarUltimo, false);
	document.getElementById("borrarprimero").addEventListener("click", borrarPrimero, false);
	document.getElementById("sustituirprimero").addEventListener("click", sustituirPrimero, false);

}, false);

function crearParrafo() {
	var texto = document.getElementById("texto");
	if (texto.value != "") {
		var nuevoP = document.createElement("p");
		var textoP = document.createTextNode(texto.value);
		nuevoP.appendChild(textoP);


		document.getElementById("div1").appendChild(nuevoP);
		document.getElementById("div1").setAttribute("class", "rojo");
	} else {
		alert("El textarea está vacio. Introduzca texto.");
	}

}

function borrarUltimo() {
	var contenedor = document.getElementById("div1");
	if (contenedor.textContent != "") {
		contenedor.removeChild(contenedor.lastChild);

	} else {
		alert("El contenedor está vacío, no hay nada que borrar.");
	}

}

function borrarPrimero() {
	var contenedor = document.getElementById("div1");
	if (contenedor.textContent != "") {
		contenedor.removeChild(contenedor.firstChild);
	} else {
		alert("El contenedor está vacío, no hay nada que borrar.");
	}
}

function sustituirPrimero() {
	var contenedor = document.getElementById("div1");
	if (contenedor.textContent != "") {
		var nuevoP = document.createElement("p");
		var textoP = document.createTextNode("Vacío");
		nuevoP.appendChild(textoP);
		contenedor.replaceChild(nuevoP, contenedor.firstChild);
	} else {
		alert("El contenedor está vacío, no hay nada que sustituir.");
	}
}

function crearImagen() {
	var contenedor = document.getElementById("div1");
	var src = document.getElementById("texto");
	if (src.value != "") {
		var alt = prompt("Introduzca nombre de la foto.");
		var img = document.createElement("img");
		img.setAttribute("src", src.value);
		img.setAttribute("alt", alt);
		contenedor.appendChild(img);

	} else {
		alert("El contenedor está vacío, no hay nada que sustituir.");
	}
}