document.addEventListener("DOMContentLoaded", function() {
	var cambio = false;
	while(!cambio)
	try {
		validacion();
		cambio = true;
	} catch (err) {
		alert(err);
	}

}, false);

function validacion() {
	var filas = parseInt(prompt("Introduzca el número de filas."));
	var columnas = parseInt(prompt("Introduzca el número de columnas."));

	if (!isNaN(filas) && !isNaN(columnas) && (filas > 0) && (columnas > 0)) {
		crearTabla(filas, columnas);
	} else {
		throw "Ambos valores han de ser de tipo númerico entero y mayores a 0.";
	}
	return;
}

function crearTabla(filas, columnas) {
	var tabla = document.createElement("table");
	tabla.classList.add("table");
	tabla.classList.add("table-dark");
	tabla.classList.add("table-bordered");
	tabla.classList.add("table-hover");

	for (let i = 0; i < filas; i++) {
		var tr = document.createElement("tr");
		for (let j = 0; j < columnas; j++) {
			var td = document.createElement("td");
			textoTd = document.createTextNode("celda en la fila " + (i+1) + " columna " + (j+1));
			td.appendChild(textoTd);
			td.addEventListener('click', activar, false);
			tr.appendChild(td);
		}
		tabla.appendChild(tr);
	}
	document.getElementById("contenedor").append(tabla);
}

function activar(){
	var input = document.createElement("input");
	input.setAttribute("type", "text");
	event.target.replaceChild(input, event.target.lastChild);
	event.target.lastChild.focus();
	event.target.removeEventListener('click', activar);
	input.addEventListener('keypress', salvarCambios, false);

}

function salvarCambios(e){
	if(e.which === 13){
		var padre = event.target.parentNode;
		var valor = event.target.value;
		event.target.removeEventListener('keypress', salvarCambios);
		event.target.parentNode.replaceChild(document.createTextNode(valor), event.target);
		padre.addEventListener('click', activar, false);
	}
	

}

