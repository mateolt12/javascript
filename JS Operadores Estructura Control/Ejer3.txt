function mayor(n1, n2, n3) {

    if (isNaN(n1) || isNaN(n2) || isNaN(n3)) {

        return ("Alguno de los n�meros no es v�lido. ");

    } else {
        max = n1;
        min = n3;


        if (n2 > n1) {
            max = n2;
            if (n3 > max) {
                max = n3;
                return "El mayor es: " + max;

            } else {
                return "El mayor es: " + n2;

            }

        } else if (n3 > n1) {
            max = n3;
            return "El mayor es: " + max;

        } else {
            return "El mayor es: " + n1;

        }

    }
}

window.onload = function () {
    p1 = prompt("Numero 1");
    p2 = prompt("Numero 2");
    p3 = prompt("Numero 3");
    document.getElementById("numeros").innerHTML = "Los n�meros introducidos son: " + p1 + ", " + p2 + " y " + p3;

    document.getElementById("resultado").innerHTML = mayor(p1, p2, p3);
}

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Mayor de tres n�meros</title> 
        <script src="lib_maymin.js"> </script>
    </head>
    <body>
        <noscript><p>Esta p�gina requiere JavaScript para su correcto
            funcionamiento. Compruebe si est� deshabilitado en el navegador.</p>
        </noscript>
        <p id="numeros"></p>
        <p id="resultado"></p>
    </body>
</html>