window.onload = function() {
modulos = document.getElementById("daw");

	document.getElementById("add").addEventListener("click", function(e) {
		e.preventDefault();
		añadir();
	}, false);
	document.getElementById("del").addEventListener("click", function(e) {
		e.preventDefault();
		borrar();
	}, false);
}


// Borrar los elementos seleccionados
function borrar() {

	for (let i = (modulos.length - 1); i >= 0; i--) {
		modulos.remove(modulos.selectedIndex);

	}


}


// Devolver true si el dato ya está en la lista
function repetido(mods, mod) {
	for (let i = 0; i < mods.length; i++) {
		if (mods[i].text == mod) {
			return true;
		}
	}
	return false;
}


// Devolver la posición adecuada para la inserción
function buscarPos(mods, mod) {
	for (let i = 0; i < mods.length; i++) {
		if (mod < mods[i].text) {
			return i;
		}

	}

}


// Añadir en la posición correcta por orden alfabético y en mayúsculas
function añadir() {
	// Obtener datos
	var elegirModulo = document.getElementById("modulos");
	var nuevoMod = elegirModulo.options[elegirModulo.selectedIndex].text;
	if (repetido(modulos, nuevoMod)) {
		alert("Datos repetido.");
	} else {
		// Insertar un nuevo OPTION en la posición devuelta por buscarPos()
		var posicion = buscarPos(modulos, nuevoMod);
		var opcion = document.createElement("option");
		opcion.text = nuevoMod;
		modulos.add(opcion, posicion); 


	}
}