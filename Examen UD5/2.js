var max = 0;


window.onload = function() {
	max = prompt("Nº máximo de caracteres:");
	inputs = document.getElementsByTagName("input");
	var campos = new Array("nom", "ape", "dir", "pro");

	if (window.addEventListener) { // All browsers except IE before version 9
		for (i in campos) {
			document.getElementById(campos[i] + "prog").max = max;
			document.getElementById(campos[i] + "prog").value = 0;
			document.getElementById(campos[i] + "prog").addEventListener("click", resetear, false);
			document.getElementById(campos[i]).addEventListener("mouseover", function() {
				this.style.backgroundColor = 'lightgreen';
			}, false);
			document.getElementById(campos[i]).addEventListener("mouseout", function() {
				this.style.backgroundColor = 'inherit';
			}, false);

			document.getElementById(campos[i]).addEventListener("keypress", cambiarTexto, false);
			document.getElementById(campos[i]).addEventListener("keydown", cambiarCampo, false);
		}
	}
}


function cambiarTexto(e) {
	if (e.keyCode != 13) {
		this.setAttribute("maxLength", max); //This es el input en este caso.
		var barra = document.getElementById(this.id + "prog"); //This (input) por eso al this.id le sumo "prog" para obtener el elemento barra.
		if (barra.value > this.value.length) {
			barra.value = this.value.length;
		}
		barra.value++;
		if (this.value.length == max) {
			alert("No puedes escribir mas!");
		}

	}


}


function resetear() {
	this.value = 0; // this es la barra de progreso. (This es siempre la barra pork esta funcion solo se asigna al evento de las barras, no a los inputs.)
	document.getElementById(this.id.substring(0, 3)).value = ""; //this.id.substring(0, 3) devuelve el objeto correspondiente a la barra de progreso.
}


function cambiarCampo(e) {

	var pos = obtenerPosicion(this);

	if (e.which == 13) {
		alert(this.value);
	}

	if (e.which == 40) {
		if (pos != (inputs.length - 1)) {
			inputs[pos + 1].focus();
		}
	}

	if (e.which == 38) {
		if (pos != 0) {
			inputs[pos - 1].focus();
		}
	}
}

function obtenerPosicion(input) {
	for (let i = 0; i < inputs.length; i++) {
		if (input === inputs[i]) {
			return i;
		}
	}
}