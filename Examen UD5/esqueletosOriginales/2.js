var max = 0;


window.onload = function () {
	max = prompt("Nº máximo de caracteres:");
	var campos = new Array("nom","ape","dir","pro");
	
	if(window.addEventListener) { // All browsers except IE before version 9
		for (i in campos) {
			document.getElementById(campos[i]+"prog").max = max;
			document.getElementById(campos[i]+"prog").value = 0;
			document.getElementById(campos[i]+"prog").addEventListener("click", resetear, false);

			document.getElementById(campos[i]).addEventListener("keypress", cambiarTexto, false);
			document.getElementById(campos[i]).addEventListener("keydown", cambiarCampo, false);
		}
	}
}


function cambiarTexto(e) {
	// Actualizar contenido de campos de texto y barras de progreso
}


function resetear() {
	// Resetear contenido de campos de texto y barras de progreso
}


function cambiarCampo(e) {
	// Cambiar campo activo
	// Mostrar alerta
}
